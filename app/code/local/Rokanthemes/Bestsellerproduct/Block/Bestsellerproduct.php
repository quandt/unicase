<?php
class Rokanthemes_Bestsellerproduct_Block_Bestsellerproduct extends Mage_Catalog_Block_Product_Abstract
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	
    protected function useFlatCatalogProduct()
    {
        return Mage::getStoreConfig('catalog/frontend/flat_catalog_product');
    }
	public function getConfig($app) 
	{
		$config = Mage::getStoreConfig('bestsellerproduct');
		if (isset($config['bestsellerproduct_config']) ) {
			$value = $config['bestsellerproduct_config'][$app];
			return $value;
		} else {
			throw new Exception($app.' value not set');
		}
	} 
	public function getProducts()
    {	
		$_rootcatID = Mage::app()->getStore()->getRootCategoryId();
		$coll_bestseller = Mage::getResourceModel('bestsellerproduct/product_bestseller')->addOrderedQty()->joinField('category_id','catalog/category_product','category_id','product_id=entity_id',null,'left')->addAttributeToFilter('category_id', array('in' => $_rootcatID));
        $coll_bestseller = $this->_addProductAttributesAndPrices($coll_bestseller)->addMinimalPrice()->setOrder('ordered_qty', 'desc');
        $coll_bestseller->setPageSize($this->getConfig('qty'));

        if($this->useFlatCatalogProduct())
        {
            foreach ($coll_bestseller as $product) 
            {
                $productId = $product->_data['entity_id'];
                $_product = Mage::getModel('catalog/product')->load($productId);
                $product->_data['name']        = $_product->getName();
                $product->_data['thumbnail']   = $_product->getThumbnail();
                $product->_data['small_image'] = $_product->getSmallImage();
            }            
        }  		
	
        $this->setProductCollection($coll_bestseller);
    }
    public function getBestsellerproduct()     
    { 
        if (!$this->hasData('bestsellerproduct')) {
            $this->setData('bestsellerproduct', Mage::registry('bestsellerproduct'));
        }
        return $this->getData('bestsellerproduct');
    }
	function cut_string_bestsellerproduct($str,$num){ 
		if(strlen($str) <= $num) {
			return $string;
		}
		else {	
			if(strpos($str," ",$num) > $num){
				$new_space = strpos($str," ",$num);
				$new_string = substr($str,0,$new_space)."..";
				return $new_string;
			}
			$new_string = substr($str,0,$num)."..";
			return $new_string;
		}
	}
}